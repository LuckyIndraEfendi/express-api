const axios = require("axios");
const cheerio = require("cheerio");
const express = require("express");
const kuramanime = require("./src/routes/kuramanime/index");
const app = express();
const port = 5000;
const cors = require("cors");

app.use(cors());
app.use("/kuramanime", kuramanime);

app.get("/", (req, res) => {
  res.send("hi");
});
app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
